<?php

namespace interfaces;

interface IComputer {

    public function start();
    public function restart();
    public function shutdown();
    public function printParameters();
    public function identifyUser();

}