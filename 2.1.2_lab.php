<?php

class Computer {

    function start() {
        echo 'I\'m started!';
    }

    function shutdown() {
        echo 'I\'m shutdowning!';
    }

}

$compObj = new Computer();

$compObj->start();

if ($compObj instanceof Computer){
    $compObj->shutdown();
}
