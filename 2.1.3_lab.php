<?php

class Computer {

    const IS_DESKTOP = true;
    const IS_ON = 'on';
    const IS_OFF = 'off';
    const RESTART_INTERVAL = 5;

    var $cpu, $ram, $video, $memory, $currentStatement;

    function __construct() {
        $this->currentStatement = Computer::IS_OFF;
    }

//======================================= manipulating with pc statement
    function start() {
        if ($this->currentStatement == Computer::IS_OFF) {
            echo 'Hello! You are welcome!' . PHP_EOL;
            $this->currentStatement = Computer::IS_ON;
        } else {
            echo 'Warning: Computer is working now!' . PHP_EOL;
            return false;
        }

    }

    function shutdown() {
        if ($this->currentStatement == Computer::IS_ON) {
            echo 'Goodbye!' . PHP_EOL;
            $this->currentStatement = Computer::IS_OFF;
        } else {
            echo 'Warning: Computer is already off!' . PHP_EOL;
            return false;
        }
    }

    function restart() {
        if ($this->currentStatement == Computer::IS_ON) {
            echo 'Computer restarting now. ';
            $this->shutdown();
            for ($i = 0; $i < Computer::RESTART_INTERVAL; $i++) {
                echo '.';
                sleep(1);
            }
        } else {
            echo 'Warning: Computer is already off!' .PHP_EOL;
            return false;
        }
        echo PHP_EOL;
        $this->start();
    }

//=======================================
 //cоздать конструктор (__constract), где обозначить свойства пк
}

$acer = new Computer();

echo 'Start PC' . PHP_EOL;
$acer->start();
echo 'Restart PC' . PHP_EOL;
$acer->restart();
echo 'Shutdown PC' . PHP_EOL;
$acer->shutdown();
echo 'Restart PC' . PHP_EOL;
$acer->restart();

