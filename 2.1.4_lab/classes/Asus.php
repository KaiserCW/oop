<?php

/**
 * Created by PhpStorm.
 * User: Kaiser
 * Date: 18.02.2017
 * Time: 0:01
 */
class Asus extends Computer {

    public function __construct() {
        parent::__construct();
        $this->cpu = 'Intel Core i7-6820HK';
        $this->ram = '64Gb DDR4';
        $this->video = 'nVidia GeForce GTX 1070';
        $this->memory = '1Tb HDD + 512Gb SSD';
        $this->computerName = 'Asus';
    }
}

