<?php

class Computer {

    const IS_DESKTOP = true;
    const IS_ON = 'on';
    const IS_OFF = 'off';
    const RESTART_INTERVAL = 5;

    protected $cpu;
    protected $ram;
    protected $video;
    protected $memory;
    protected $computerName = 'Computer';
    protected $identifyMethod = 'login & password';

    private $currentStatement;

    function __construct() {
        $this->currentStatement = Computer::IS_OFF;
    }

//======================================= manipulating with pc statement
    public function start() {
        if ($this->currentStatement == Computer::IS_OFF) {
            echo 'Hello! You are welcome!' . PHP_EOL;
            $this->currentStatement = Computer::IS_ON;
        } else {
            echo 'Warning: Computer is working now!' . PHP_EOL;
            return false;
        }

    }

    public function shutdown() {
        if ($this->currentStatement == Computer::IS_ON) {
            echo 'Goodbye!' . PHP_EOL;
            $this->currentStatement = Computer::IS_OFF;
        } else {
            echo 'Warning: Computer is already off!' . PHP_EOL;
            return false;
        }
    }

    public function restart() {
        if ($this->currentStatement == Computer::IS_ON) {
            echo 'Computer restarting now. ';
            $this->shutdown();
            for ($i = 0; $i < Computer::RESTART_INTERVAL; $i++) {
                echo '.';
                sleep(1);
            }
        } else {
            echo 'Warning: Computer is already off!' .PHP_EOL;
            return false;
        }
        echo PHP_EOL;
        $this->start();
    }

    public function printParameters() {
        if ($this->currentStatement == Computer::IS_ON) {
            echo 'CPU: ' . $this->cpu . PHP_EOL;
            echo 'RAM: ' . $this->ram . PHP_EOL;
            echo 'Video: ' . $this->video . PHP_EOL;
            echo 'HDD: ' . $this->memory . PHP_EOL;
        } else {
            echo 'Error: Computer is not working now!';
        }
    }

    public function identifyUser() {
        echo $this->computerName . ' : Identify by ' . $this->identifyMethod . PHP_EOL;
    }
}