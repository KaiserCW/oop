<?php

class Lenovo extends Computer {

    const IS_DESKTOP = false;

    public function __construct() {
        parent::__construct();
        $this->cpu = 'Intel® Celeron® N3060';
        $this->ram = '4Gb DDR3';
        $this->video = 'Intel® HD Graphics';
        $this->memory = '500Gb HDD';
        $this->computerName = 'Lenovo';
        $this->identifyMethod = 'fingerprints';
    }
}