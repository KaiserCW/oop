<?php

class MacBook extends Computer {

    const IS_DESKTOP = false;

    public function __construct() {
        parent::__construct();
        $this->cpu = 'Intel Core i5';
        $this->ram = '8Gb LPDDR3';
        $this->video = 'Intel HD Graphics 6000';
        $this->memory = '128Gb SSD';
        $this->computerName = 'MacBook';
        $this->identifyMethod = 'Apple ID';
    }
}