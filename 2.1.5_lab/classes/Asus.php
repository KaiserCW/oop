<?php

class Asus extends Computer {

    public function __construct() {
        parent::__construct();
        $this->setCpu('Intel Core i7-6820HK');
        $this->setRam('64Gb DDR4');
        $this->setVideo('nVidia GeForce GTX 1070');
        $this->setMemory('1Tb HDD + 512Gb SSD');
        $this->setComputerName('Asus');
    }
}

