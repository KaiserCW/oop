<?php

class Computer {

    const IS_DESKTOP = true;
    const IS_ON = 'on';
    const IS_OFF = 'off';
    const RESTART_INTERVAL = 5;

    private $cpu;
    private $ram;
    private $video;
    private $memory;
    private $computerName = 'Computer';
    private $identifyMethod = 'login & password';

    private $currentStatement;

    function __construct() {
        $this->currentStatement = self::IS_OFF;
    }

//======================================= setters and getters for private properties

    //--------------------- COMPUTER NAME
    public function getComputerName() {
        return $this->computerName;
    }

    public function setComputerName($compName) {
        $this->computerName = $compName;
        return $this;
    }

    //--------------------- CPU
    public function getCpu() {
        return $this->cpu;
    }

    public function setCpu($cpu) {
        $this->cpu = $cpu;
        return $this;
    }

    //--------------------- RAM
    public function getRam() {
        return $this->ram;
    }

    public function setRam($ram) {
        $this->ram = $ram;
        return $this;
    }

    //--------------------- VIDEO
    public function getVideo() {
        return $this->video;
    }

    public function setVideo($video) {
        $this->video = $video;
        return $this;
    }

    //--------------------- MEMORY
    public function getMemory() {
        return $this->memory;
    }

    public function setMemory($memory) {
        $this->memory = $memory;
        return $this;
    }

    //--------------------- IDENTIFY METHOD
    public function getIdentifyMethod() {
        return $this->identifyMethod;
    }

    public function setIdentifyMethod($identifyMethod) {
        $this->identifyMethod = $identifyMethod;
        return $this;
    }

//======================================= manipulating with pc statement
    public function start() {
        if ($this->currentStatement == self::IS_OFF) {
            echo 'Hello! You are welcome!' . PHP_EOL;
            $this->currentStatement = self::IS_ON;
        } else {
            echo 'Warning: Computer is working now!' . PHP_EOL;
            return false;
        }

    }

    public function shutdown() {
        if ($this->currentStatement == self::IS_ON) {
            echo 'Goodbye!' . PHP_EOL;
            $this->currentStatement = self::IS_OFF;
        } else {
            echo 'Warning: Computer is already off!' . PHP_EOL;
            return false;
        }
    }

    public function restart() {
        if ($this->currentStatement == self::IS_ON) {
            echo 'Computer restarting now. ';
            $this->shutdown();
            for ($i = 0; $i < self::RESTART_INTERVAL; $i++) {
                echo '.';
                sleep(1);
            }
        } else {
            echo 'Warning: Computer is already off!' .PHP_EOL;
            return false;
        }
        echo PHP_EOL;
        $this->start();
    }


//======================================= print pc info
    public function printParameters() {
        if ($this->currentStatement == self::IS_ON) {
            echo $this->getCpu() . PHP_EOL;
            echo $this->getRam() . PHP_EOL;
            echo $this->getVideo() . PHP_EOL;
            echo $this->getMemory() . PHP_EOL;
        } else {
            echo 'Error: Computer is not working now!';
        }
    }

    public function identifyUser() {
        echo $this->getComputerName() . ' : Identify by ' . $this->getIdentifyMethod() . PHP_EOL;
    }
}