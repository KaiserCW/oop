<?php

class MacBook extends Computer {
    public function __construct() {
        parent::__construct();
        $this->setCpu('Intel Core i5');
        $this->setRam('8Gb LPDDR3');
        $this->setVideo('Intel HD Graphics 6000');
        $this->setMemory('128Gb SSD');
        $this->setComputerName('MacBook');
        $this->setIdentifyMethod('Apple ID');
    }
}