<?php

require_once __DIR__ . "/classes/Computer.php";
require_once __DIR__ . "/classes/Asus.php";
require_once __DIR__ . "/classes/Lenovo.php";
require_once __DIR__ . "/classes/MacBook.php";

$objAsus = new Asus();
$objLenovo = new Lenovo();
$objMacBook = new MacBook();

$objAsus->start();
$objAsus->identifyUser();
$objAsus->printParameters();
$objAsus->shutdown();

sleep(2);

$objLenovo->start();
$objLenovo->identifyUser();
$objLenovo->printParameters();
$objLenovo->shutdown();

sleep(2);

$objMacBook->start();
$objMacBook->identifyUser();
$objMacBook->printParameters();
$objMacBook->shutdown();