<?php

require_once __DIR__ . "/classes/Computer.php";
require_once __DIR__ . "/classes/computers/Asus.php";
require_once __DIR__ . "/classes/computers/Lenovo.php";
require_once __DIR__ . "/classes/computers/MacBook.php";

$objAsus = new Asus();
$objLenovo = new Lenovo();
$objMacBook = new MacBook();

$objAsus->start();
$objAsus->identifyUser();
$objAsus->printParameters();
$objAsus->restart();
$objAsus->shutdown();

echo '----------------------------------' . PHP_EOL;
sleep(2);

$objLenovo->start();
$objLenovo->identifyUser();
$objLenovo->printParameters();
$objLenovo->restart();
$objLenovo->shutdown();

echo '----------------------------------' . PHP_EOL;
sleep(2);

$objMacBook->start();
$objMacBook->identifyUser();
$objMacBook->printParameters();
$objMacBook->restart();
$objMacBook->shutdown();