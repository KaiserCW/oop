<?php

class Asus extends Computer implements IComputer {

    public function __construct() {
        parent::__construct();
        $this->setCpu('Intel Core i7-6820HK');
        $this->setRam('64Gb DDR4');
        $this->setVideo('nVidia GeForce GTX 1070');
        $this->setMemory('1Tb HDD + 512Gb SSD');
        $this->setComputerName('Asus');
    }

    public function identifyUser() {
        echo $this->getComputerName() . ' : Identify by ' . $this->getIdentifyMethod() . PHP_EOL;
    }

}

