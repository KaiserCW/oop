<?php

class Lenovo extends Computer implements IComputer {
    public function __construct() {
        parent::__construct();
        $this->setCpu('Intel Celeron N3060');
        $this->setRam('4Gb DDR3');
        $this->setVideo('Intel HD Graphics');
        $this->setMemory('500Gb HDD');
        $this->setComputerName('Lenovo');
        $this->setIdentifyMethod('fingerprints');
    }

    public function identifyUser() {
        echo $this->getComputerName() . ' : Identify by ' . $this->getIdentifyMethod() . PHP_EOL;
    }

}