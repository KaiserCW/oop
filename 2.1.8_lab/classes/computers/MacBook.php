<?php

class MacBook extends Computer implements IComputer {
    public function __construct() {
        parent::__construct();
        $this->setCpu('Intel Core i5');
        $this->setRam('8Gb LPDDR3');
        $this->setVideo('Intel HD Graphics 6000');
        $this->setMemory('128Gb SSD');
        $this->setComputerName('MacBook');
        $this->setIdentifyMethod('Apple ID');
    }

    public function identifyUser() {
        echo $this->getComputerName() . ' : Identify by ' . $this->getIdentifyMethod()  . PHP_EOL;
    }



}