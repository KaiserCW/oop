<?php

class Autoload {

    public static function loader($className) {
        $pathToFile = __DIR__ . '/application/' . $className . '.php';
        if(is_readable($pathToFile)) {
            require_once $pathToFile;
            return true;
        }
        return false;
    }

}