<?php

require_once __DIR__ . "/../helpers/Console.php";

abstract class Computer {

    const IS_DESKTOP = true;
    const IS_ON = 'on';
    const IS_OFF = 'off';
    const RESTART_INTERVAL = 5;

    private $cpu;
    private $ram;
    private $video;
    private $memory;
    private $computerName = 'Computer';
    private $identifyMethod = 'login & password';

    private $currentStatement;

    function __construct() {
        $this->currentStatement = self::IS_OFF;
    }

//======================================= setters and getters for private properties

    //--------------------- COMPUTER NAME
    public function getComputerName() {
        return $this->computerName;
    }

    public function setComputerName($computerName) {
        $this->computerName = $computerName;
        return $this;
    }

    //--------------------- CPU
    public function getCpu() {
        return $this->cpu;
    }

    public function setCpu($cpu) {
        $this->cpu = $cpu;
        return $this;
    }

    //--------------------- RAM
    public function getRam() {
        return $this->ram;
    }

    public function setRam($ram) {
        $this->ram = $ram;
        return $this;
    }

    //--------------------- VIDEO
    public function getVideo() {
        return $this->video;
    }

    public function setVideo($video) {
        $this->video = $video;
        return $this;
    }

    //--------------------- MEMORY
    public function getMemory() {
        return $this->memory;
    }

    public function setMemory($memory) {
        $this->memory = $memory;
        return $this;
    }

    //--------------------- IDENTIFY METHOD
    public function getIdentifyMethod() {
        return $this->identifyMethod;
    }

    public function setIdentifyMethod($identifyMethod) {
        $this->identifyMethod = $identifyMethod;
        return $this;
    }

//======================================= manipulating with pc statement
    public function start() {
        if ($this->currentStatement == self::IS_OFF) {
            Console::printLine('Hello! You are welcome!', 'SUCCESS');;
            $this->currentStatement = self::IS_ON;
        } else {
            Console::printLine('Computer is working now!', 'WARNING');
            return false;
        }

    }

    function shutdown() {
        if ($this->currentStatement == Computer::IS_ON) {
            Console::printLine('Goodbye!', 'SUCCESS');
            $this->currentStatement = Computer::IS_OFF;
        } else {
            Console::printLine('Computer is already off!', 'FAILURE');
            return false;
        }
    }

    function restart() {
        if ($this->currentStatement == Computer::IS_ON) {
            Console::printLine('Computer restarting now. ');
            $this->shutdown();
            for ($i = 0; $i < Computer::RESTART_INTERVAL; $i++) {
                echo '.';
                sleep(1);
            }
        } else {
            Console::printLine(' Computer is already off!', 'FAILURE');
            return false;
        }
        echo PHP_EOL;
        $this->start();
    }


//======================================= print pc info
    public function printParameters() {
        if ($this->currentStatement == self::IS_ON) {
            echo 'CPU: '    . $this->getCpu()    . PHP_EOL;
            echo 'RAM: '    . $this->getRam()    . PHP_EOL;
            echo 'Video: '  . $this->getVideo()  . PHP_EOL;
            echo 'Memory: ' . $this->getMemory() . PHP_EOL;
        } else {
            echo 'Error: Computer is not working now!';
        }
    }

    abstract public function identifyUser();

}