<?php

class Autoload {

    public static function loadClass($className) {
        $classPath = str_replace('\\', '/', $className);
        $classPath = __DIR__ . '/' . $classPath . '.php';
        require_once $classPath;
    }

}