<?php

namespace classes;

class Console {
    static private $success = "SUCCESS";
    static private $failure = "FAILURE";
    static private $warning = "WARNING";
    static private $note = "NOTE";

    static public function printLine($message, $status = '') {
        $message = (string) $message;

        switch ($status) {
            case self::$success:
                $prefix = 'Success: ';
                break;
            case self::$warning:
                $prefix = 'Warning: ';
                break;
            case self::$failure:
                $prefix = 'Failure: Please try again later.';
                break;
            case self::$note:
            default:
                $prefix = '';
        }

        echo $prefix . $message . PHP_EOL;

    }
}