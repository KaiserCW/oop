<?php

/*require_once __DIR__ . "/interfaces/IComputer.php";
require_once __DIR__ . "/classes/Computer.php";
require_once __DIR__ . "/classes/computers/Asus.php";
require_once __DIR__ . "/classes/computers/Lenovo.php";
require_once __DIR__ . "/classes/computers/MacBook.php";
require_once __DIR__ . "/Console.php";*/

require_once __DIR__ . '/Autoload.php';
spl_autoload_register('Autoload::loadClass');

$objAsus = new \classes\computers\Asus();
$objLenovo = new \classes\computers\Lenovo();
$objMacBook = new \classes\computers\MacBook();

$objAsus->start();
$objAsus->identifyUser();
$objAsus->printParameters();
$objAsus->restart();
$objAsus->shutdown();

echo '----------------------------------' . PHP_EOL;
sleep(2);

$objLenovo->start();
$objLenovo->identifyUser();
$objLenovo->printParameters();
$objLenovo->restart();
$objLenovo->shutdown();

echo '----------------------------------' . PHP_EOL;
sleep(2);

$objMacBook->start();
$objMacBook->identifyUser();
$objMacBook->printParameters();
$objMacBook->restart();
$objMacBook->shutdown();